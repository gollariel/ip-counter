## IP Counter ##

This is a tiny microservice that provides functionality to calculate unique IPs in memory from provided logs.
When we run service we take all configs from environment variables (could be not the best idea if we want to reload configs or we will have a thousand of configs but looks pretty nice for API service configuration)

*Attention!* This repository contains some utils what should be in the engine and should not be part of microservice.
For example, I used fasthttp implementation instead of a native golang HTTP client. For small service, it`s an excellent solution. But fasthttp does not have any good muxers, therefore I add my own implementation. This implementation should be moved to a separate library. Also, I use zap logger and my own wrapper. This is also should be in another repository. Also, I implemented a "data set" algorithm inside this repository, but we could use data set in other microservices as well.

### Performance ###

To reach good performance I do not use popular solutions. It's helped me to make API with average latency ~3ms for `/logs` and ~4ms for `/metrics`

### Set up ###

The fastest way to set up service - it's run it in the docker. For that I prepared Dockerfile. Also, service waits for some environment variables:
`LOGGER_ADDR=127.0.0.1:5000`
`PROMETHEUS_ADDR=127.0.0.1:9102`

### TODO ###
Some things should be improved

- We still do not have enough coverage. I expect to see at least 70%