FROM golang:1.14-buster as build
WORKDIR /go/src/bitbucket.org/gollariel/ip-counter
COPY . .
ENV GOFLAGS=-mod=vendor
RUN make ip_counter
RUN cp ./bin/ip_counter /binary

FROM ubuntu:latest
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /binary .
RUN chmod +x /binary
ENTRYPOINT ["/binary"]