test:
	GOCONFIG=config/local_test/ go test -timeout 10s -race -p 1 ./...

test-cover:
	GOCONFIG=config/local_test/ go test -cover -p 1 ./...

format:
	gofmt -s -w **/*.go

vendor:
	go mod tidy
	go mod vendor
	modvendor -copy="**/*.c **/*.h **/*.proto **/*.m" -v

init:
	chmod -R +x .githooks/
	mkdir -p .git/hooks/
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

lint:
	golangci-lint run --exclude-use-default=false --skip-dirs=vendor --disable-all --enable=goimports --enable=gosimple --enable=typecheck --enable=unused --enable=golint --enable=deadcode --enable=structcheck --enable=varcheck --enable=errcheck --enable=ineffassign --enable=govet --enable=staticcheck --enable=gofmt --deadline=3m ./...

ip_counter:
	CGO_ENABLED=0 env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o bin/ip_counter ./cmd/counter

.PHONY: vendor test test-cover format init lint