package models

import (
	"encoding/json"
	"testing"

	"bitbucket.org/gollariel/ip-counter/testutils"
)

func TestInputLog(t *testing.T) {
	raw := []byte(`{ "timestamp": "2020-06-24T15:27:00.123456Z", "ip": "83.150.59.250", "url": "http://example.org" }`)
	var l InputLog
	testutils.Equal(t, nil, json.Unmarshal(raw, &l))
	testutils.Equal(t, "83.150.59.250", l.IP)
	testutils.Equal(t, "http://example.org", l.URL)
	testutils.Equal(t, int64(1593012420), l.Timestamp.Unix())

	d, err := json.Marshal(l)
	testutils.Equal(t, nil, err)
	testutils.EqualJSON(t, []byte(`{ "timestamp": "2020-06-24T15:27:00.123456Z", "ip": "83.150.59.250", "url": "http://example.org" }`), d)
}

/*
BenchmarkInputLogWithoutEasyJSON-12              732272              1633 ns/op             280 B/op          6 allocs/opp
BenchmarkInputLog-12                             784020              1515 ns/op             264 B/op          5 allocs/op
*/
func BenchmarkInputLog(b *testing.B) {
	raw := []byte(`{ "timestamp": "2020-06-24T15:27:00.123456Z", "ip": "83.150.59.250", "url": "http://example.org" }`)
	var l InputLog
	for i := 0; i < b.N; i++ {
		err := json.Unmarshal(raw, &l)
		if err != nil {
			b.Fatal(err)
		}
	}
}
