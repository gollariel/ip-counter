package models

import "time"

// InputLog easyjson input log model
type InputLog struct {
	Timestamp time.Time `json:"timestamp,omitempty"`
	IP        string    `json:"ip,omitempty"`
	URL       string    `json:"url,omitempty"`
}
