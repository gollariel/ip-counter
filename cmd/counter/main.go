package main

import (
	"bitbucket.org/gollariel/ip-counter/fastlog"
	"bitbucket.org/gollariel/ip-counter/fastserver"
	"bitbucket.org/gollariel/ip-counter/service"
	"bitbucket.org/gollariel/ip-counter/utils"
)

// Env prefixes
const (
	PrometheusEnvSuffix = "PROMETHEUS"
	LoggerEnvSuffix     = "LOGGER"
)

func main() {
	err := utils.RunGopsInstance()
	if err != nil {
		fastlog.Error("Error run gops instance", "err", err)
		return
	}

	prometheusConfig, err := fastserver.GetConnectionConfigFromEnv(PrometheusEnvSuffix)
	if err != nil {
		fastlog.Error("Error not found prometheus configs", "err", err)
		return
	}

	loggerConfig, err := fastserver.GetConnectionConfigFromEnv(LoggerEnvSuffix)
	if err != nil {
		fastlog.Error("Error not found logger config", "err", err)
		return
	}

	logsRoot := service.GetLogMux()
	prometheusRoot := service.GetPrometheusMux()
	go func() {
		if err := fastserver.ListenAndServe(prometheusRoot.Handler(nil), prometheusConfig); err != nil {
			fastlog.Fatal("Failed to start prometheus fasthttp server", "err", err)
		}
	}()
	if err := fastserver.ListenAndServe(logsRoot.Handler(nil), loggerConfig); err != nil {
		fastlog.Fatal("Failed to start fasthttp logger server", "err", err)
	}
}
