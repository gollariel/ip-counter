module bitbucket.org/gollariel/ip-counter

go 1.14

require (
	github.com/google/gops v0.3.6
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mailru/easyjson v0.7.1
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/satori/go.uuid v1.2.0
	github.com/valyala/fasthttp v1.15.1
	go.uber.org/zap v1.15.0
)
