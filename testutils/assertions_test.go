package testutils

import (
	"errors"
	"fmt"
	"testing"

	werrors "github.com/pkg/errors"
)

func TestEqual(t *testing.T) {
	Equal(t, "test msg", "test msg")
	Equal(t, 0.654, 0.654)
	Equal(t, nil, nil)
	Equal(t, errors.New("err"), errors.New("err"))
	err := errors.New("test error")
	Equal(t, err, werrors.Wrap(err, "additional data"))
	Equal(t, err, fmt.Errorf("test: %w", err))
	Equal(t, err, fmt.Errorf("test2: %w", fmt.Errorf("test: %w", err)))
	Equal(t, fmt.Errorf("test2: %w", fmt.Errorf("test: %w", err)), fmt.Errorf("test2: %w", fmt.Errorf("test: %w", err)))
}
