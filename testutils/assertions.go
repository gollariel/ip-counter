package testutils

import (
	"encoding/json"
	nerrors "errors"
	"math"
	"reflect"
	"strings"
	"testing"

	"github.com/pkg/errors"
)

// ApproximatelyEqual function to test if two real numbers are (almost) equal
func ApproximatelyEqual(a, b float64) bool {
	epsilon := math.SmallestNonzeroFloat64
	difference := a - b
	return difference < epsilon && difference > -epsilon
}

// Equal checks if two variables are equal
func Equal(t *testing.T, expected, received interface{}) {
	t.Helper()
	switch v := received.(type) {
	case float64:
		e, ok := expected.(float64)
		if !ok {
			t.Fatalf("expected value is not float64: %+v expected: %+v", received, expected)
		}

		if !ApproximatelyEqual(v, e) {
			t.Fatalf("mismatched float, received: %f, expected: %f", received, expected)
		}
	case float32:
		e, ok := expected.(float32)
		if !ok {
			t.Fatalf("expected value is not float32: %+v expected: %+v", received, expected)
		}

		if !ApproximatelyEqual(float64(v), float64(e)) {
			t.Fatalf("mismatched float, received: %f, expected: %f", received, expected)
		}
	case error:
		e, ok := expected.(error)
		if !ok {
			t.Fatalf("expected value is not error: %+v expected: %+v", received, expected)
		}

		if !nerrors.Is(v, e) && errors.Cause(v) != errors.Cause(e) && !strings.EqualFold(v.Error(), e.Error()) {
			t.Fatalf("mismatched errors, received: %+v expected: %+v", received, expected)
		}
	default:
		if !reflect.DeepEqual(received, expected) {
			t.Fatalf("mismatched values: %v != %v", received, expected)
		}
	}
}

// EqualJSON compare JSONs
func EqualJSON(t *testing.T, expected, received []byte) {
	t.Helper()
	if len(expected) == 0 {
		if len(received) != 0 {
			t.Fatalf("expected empty string, got: %s", received)
		}
		return
	}
	rec := map[string]interface{}{}
	if err := json.Unmarshal(received, &rec); err != nil {
		t.Fatalf("Unable to parse json: %s", received)
	}

	exp := map[string]interface{}{}
	if err := json.Unmarshal(received, &exp); err != nil {
		t.Fatalf("Unable to parse json: %s", expected)
	}

	Equal(t, rec, exp)
}
