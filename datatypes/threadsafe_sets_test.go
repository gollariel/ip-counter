package datatypes

import (
	"sort"
	"strings"
	"testing"

	"bitbucket.org/gollariel/ip-counter/testutils"
)

func TestNewThreadSafeDataSetStrings(t *testing.T) {
	testcases := map[string]struct {
		keys             []string
		shouldContain    []string
		shouldnotContain []string
	}{
		"empty": {
			keys:             []string{},
			shouldContain:    []string{},
			shouldnotContain: []string{" ", "abc"},
		},
		"valid": {
			keys: []string{
				"one",
				"two",
				"three",
			},
			shouldContain:    []string{"one", "two", "three"},
			shouldnotContain: []string{"four", "", " ", ","},
		},
	}

	for exampleName, example := range testcases {
		t.Run(exampleName, func(t *testing.T) {
			set := NewThreadSafeDataSetStrings(example.keys...)

			if len(example.shouldContain) != set.Count() {
				t.Fatal("Invalid set length")
			}

			for _, key := range example.shouldContain {
				if !set.Contains(key) {
					t.Fatalf("expected set: %v to contain: %s", set, key)
				}
			}

			for _, key := range example.shouldnotContain {
				if set.Contains(key) {
					t.Fatalf("expected set: %v to not contain: %s", set, key)
				}
			}

			res := set.ToSlice()
			sort.Slice(example.keys, func(i, j int) bool {
				return strings.Compare(example.keys[i], example.keys[j]) <= 0
			})
			sort.Slice(res, func(i, j int) bool {
				return strings.Compare(res[i], res[j]) <= 0
			})
			testutils.Equal(t, res, example.keys)
		})
	}
}

func BenchmarkThreadSafeDataSetStrings(b *testing.B) {
	d := NewThreadSafeDataSetStrings()
	for i := 0; i < b.N; i++ {
		if !d.Contains("test") {
			d.Add("test")
			d.Delete("test")
		}
	}
}
