package datatypes

import (
	"sync"
)

// ThreadSafeDataSetStrings thread safe string data set
type ThreadSafeDataSetStrings struct {
	set DataSetStrings
	mu  sync.RWMutex
}

// NewThreadSafeDataSetStrings return new thread safe data set
func NewThreadSafeDataSetStrings(input ...string) *ThreadSafeDataSetStrings {
	return &ThreadSafeDataSetStrings{
		set: NewDataSetStrings(input...),
	}
}

// Reset reset data set
func (u *ThreadSafeDataSetStrings) Reset() {
	u.mu.Lock()
	u.set = NewDataSetStrings()
	u.mu.Unlock()
}

// Add call thread safe add function
func (u *ThreadSafeDataSetStrings) Add(key string) {
	u.mu.Lock()
	u.set.Add(key)
	u.mu.Unlock()
}

// Delete call thread safe delete function
func (u *ThreadSafeDataSetStrings) Delete(key string) {
	u.mu.Lock()
	u.set.Delete(key)
	u.mu.Unlock()
}

// Count count data in set
func (u *ThreadSafeDataSetStrings) Count() int {
	u.mu.RLock()
	c := len(u.set)
	u.mu.RUnlock()
	return c
}

// ToSlice call thread safe to slice function
func (u *ThreadSafeDataSetStrings) ToSlice() []string {
	u.mu.RLock()
	s := u.set.ToSlice()
	u.mu.RUnlock()
	return s
}

// Contains call thread safe contains function
func (u *ThreadSafeDataSetStrings) Contains(key string) bool {
	u.mu.RLock()
	exists := u.set.Contains(key)
	u.mu.RUnlock()
	return exists
}

// SafeCall call thread safe callback function
func (u *ThreadSafeDataSetStrings) SafeCall(f func(strings DataSetStrings)) {
	u.mu.Lock()
	f(u.set)
	u.mu.Unlock()
}
