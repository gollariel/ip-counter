package datatypes

// DataSetStrings is a set of strings
// It can test for in O(1) time
type DataSetStrings map[string]struct{}

// NewDataSetStrings returns a new DataSetStrings from the given data
func NewDataSetStrings(input ...string) DataSetStrings {
	set := DataSetStrings{}
	for _, v := range input {
		set.Add(v)
	}
	return set
}

// Add inserts the given value into the set
func (set DataSetStrings) Add(key string) {
	set[key] = struct{}{}
}

// Delete delete value
func (set DataSetStrings) Delete(key string) {
	delete(set, key)
}

// Contains tests the membership of given key in the set
func (set DataSetStrings) Contains(key string) (exists bool) {
	_, exists = set[key]
	return
}

// ToSlice return slice of entities
func (set DataSetStrings) ToSlice() []string {
	s := make([]string, len(set))
	var i int
	for k := range set {
		s[i] = k
		i++
	}
	return s
}
