package service

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/gollariel/ip-counter/datatypes"
	"bitbucket.org/gollariel/ip-counter/fastlog"
	"bitbucket.org/gollariel/ip-counter/fastserver"
	"bitbucket.org/gollariel/ip-counter/fastserver/middlewares"
	"bitbucket.org/gollariel/ip-counter/models"
	"github.com/valyala/fasthttp"
)

var ips = datatypes.NewThreadSafeDataSetStrings()

// LogHandler handle http request to increase prometheus metric
func LogHandler(ctx *fasthttp.RequestCtx) {
	response := fastserver.NewResponseJSON()
	var request models.InputLog
	err := json.Unmarshal(ctx.PostBody(), &request)
	if err != nil {
		fastlog.Error("Failed unmarshal request struct", "err", err)
		response.SetStatus(http.StatusInternalServerError).Add("error", err.Error()).Send(ctx)
		return
	}
	ips.SafeCall(func(d datatypes.DataSetStrings) {
		if d.Contains(request.IP) {
			d.Add(request.IP)
			metric.Inc()
		}
	})
	response.SetStatus(http.StatusOK).Send(ctx)
}

// GetLogMux return routing for log aggregator
func GetLogMux() *fastserver.Index {
	middleware := fastserver.NewFastMiddleware()
	middleware.AddChain(middlewares.Recover)
	logsRoot := fastserver.Root()
	logsRoot.Add("/logs", fastserver.NewRoute(http.MethodPost, middleware.Then(LogHandler)))
	return logsRoot
}
