package service

import (
	"bitbucket.org/gollariel/ip-counter/fastserver"
	"bitbucket.org/gollariel/ip-counter/fastserver/middlewares"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
)

var metric = promauto.NewCounter(prometheus.CounterOpts{
	Name: "unique_ip_addresses",
	Help: "Calculate unique ip addresses",
})

// GetPrometheusMux return routing for prometheus
func GetPrometheusMux() *fastserver.Index {
	middleware := fastserver.NewFastMiddleware()
	middleware.AddChain(middlewares.Recover)
	prometheusRoot := fastserver.Root()
	prometheusRoot.Add("/metrics", fastserver.NewRoute("", middleware.Then(fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler()))))
	return prometheusRoot
}
