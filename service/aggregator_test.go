package service

import (
	"encoding/json"
	"net/http"
	"testing"

	"bitbucket.org/gollariel/ip-counter/models"
	"bitbucket.org/gollariel/ip-counter/testutils"
	"github.com/valyala/fasthttp"
)

func TestAggregator(t *testing.T) {
	m := GetLogMux()
	handler, _, _ := m.Get(http.MethodPost, "/logs")
	l := models.InputLog{
		IP: "127.0.0.1",
	}
	d, err := json.Marshal(l)
	testutils.Equal(t, nil, err)
	req := fasthttp.Request{}
	req.SetBody(d)
	testutils.Equal(t, false, ips.Contains("127.0.0.1"))
	handler(&fasthttp.RequestCtx{
		Request: req, //nolint
	})
	testutils.Equal(t, true, ips.Contains("127.0.0.1"))
}

/*
BenchmarkAggregator-12    	  271606	      3849 ns/op
BenchmarkPrometheus-12    	    7130	    155686 ns/op
*/
func BenchmarkAggregator(b *testing.B) {
	b.StopTimer()
	m := GetLogMux()
	handler, _, _ := m.Get(http.MethodPost, "/logs")
	l := models.InputLog{
		IP: "127.0.0.1",
	}
	d, err := json.Marshal(l)
	if err != nil {
		panic(err)
	}
	req := fasthttp.Request{}
	req.SetBody(d)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		handler(&fasthttp.RequestCtx{
			Request: req, //nolint
		})
	}
}

func BenchmarkPrometheus(b *testing.B) {
	b.StopTimer()
	m := GetPrometheusMux()
	handler, _, _ := m.Get("", "/metrics")
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		handler(&fasthttp.RequestCtx{})
	}
}
