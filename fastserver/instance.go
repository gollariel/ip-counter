package fastserver

import (
	"github.com/valyala/fasthttp"
)

// ListenAndServe start fasthttp listener
func ListenAndServe(handler fasthttp.RequestHandler, config *ConnectionConfig) error {
	httpServer := fasthttp.Server{
		Handler:      handler,
		ReadTimeout:  config.ReadTimeout,
		WriteTimeout: config.WriteTimeout,
	}

	return httpServer.ListenAndServe(config.Addr)
}
