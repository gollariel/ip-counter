package fastserver

import (
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/gollariel/ip-counter/fastlog"
	"github.com/valyala/fasthttp"
)

// ResponseJSON default response
type ResponseJSON struct {
	Data       map[string]interface{} `json:"data"`
	ServerInfo map[string]interface{} `json:"serverInfo"`
	status     int
	header     map[string]string
}

// NewResponseJSON return new json response
func NewResponseJSON() *ResponseJSON {
	return &ResponseJSON{
		Data: map[string]interface{}{},
		ServerInfo: map[string]interface{}{
			"timestamp": time.Now().Unix(),
		},
		status: http.StatusOK,
		header: map[string]string{},
	}
}

// Set set Status and prepared Data
func (r *ResponseJSON) Set(data map[string]interface{}, status int) *ResponseJSON {
	r.Data = data
	r.status = status
	return r
}

// Add add data into response
func (r *ResponseJSON) Add(key string, value interface{}) *ResponseJSON {
	r.Data[key] = value
	return r
}

// Header add header
func (r *ResponseJSON) Header(key string, value string) *ResponseJSON {
	r.header[key] = value
	return r
}

// SetStatus set status
func (r *ResponseJSON) SetStatus(status int) *ResponseJSON {
	r.status = status
	return r
}

// Debug add Data to serverInfor
func (r *ResponseJSON) Debug(debug map[string]interface{}) *ResponseJSON {
	for k, v := range debug {
		r.ServerInfo[k] = v
	}
	return r
}

// Send send json response
func (r *ResponseJSON) Send(ctx *fasthttp.RequestCtx) {
	data, err := json.Marshal(r)
	if err != nil {
		fastlog.Error("Error marshal data", "err", err)
		ctx.SetBodyString(err.Error())
		ctx.SetStatusCode(http.StatusInternalServerError)
	}
	ctx.Response.Header.Add("Content-Type", "application/json; charset=utf-8")
	for k, v := range r.header {
		ctx.Response.Header.Add(k, v)
	}
	ctx.SetStatusCode(r.status)
	ctx.SetBody(data)
}

// Send send response
func Send(ctx *fasthttp.RequestCtx, contentType string, status int, data []byte, header map[string]string) {
	ctx.SetStatusCode(status)
	ctx.Response.Header.Add("Content-Type", contentType)
	for k, v := range header {
		ctx.Response.Header.Add(k, v)
	}
	ctx.SetBody(data)
}
