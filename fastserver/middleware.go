package fastserver

import "github.com/valyala/fasthttp"

// Middleware contains methods to call before handle request
type Middleware struct {
	chains []Chain
}

// Chain single chain
type Chain func(h fasthttp.RequestHandler) fasthttp.RequestHandler

// NewFastMiddleware return new middleware
func NewFastMiddleware(c ...Chain) *Middleware {
	return &Middleware{
		chains: c,
	}
}

// AddChain add middleware to execute
func (m *Middleware) AddChain(c Chain) {
	m.chains = append(m.chains, c)
}

// Then return router handler
func (m *Middleware) Then(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	for i := range m.chains {
		h = m.chains[len(m.chains)-1-i](h)
	}
	return h
}
