package fastserver

import (
	"fmt"
	"net/http"
	"testing"

	"bitbucket.org/gollariel/ip-counter/testutils"
	"github.com/valyala/fasthttp"
)

func hello(ctx *fasthttp.RequestCtx) {
	fmt.Println("Call")
	fmt.Fprint(ctx, "Welcome!\n")
}

func hello2(ctx *fasthttp.RequestCtx) {
	fmt.Println("Call2")
	fmt.Fprint(ctx, "Welcome2!\n")
}

func hello3(ctx *fasthttp.RequestCtx) {
	fmt.Println("Call3")
	fmt.Fprint(ctx, "Welcome3!\n")
}

func TestIndex(t *testing.T) {
	i := Root()
	i.Add("/address/:name/:id/:type", NewRoute("", hello))
	i.Add("/address/:name/:id", NewRoute(http.MethodGet, hello2))
	i.Add("/s/*", NewRoute(http.MethodPost, hello3))

	r, values, e := i.Get(MethodAny, "/address/max/123/request")
	testutils.Equal(t, r != nil, true)
	testutils.Equal(t, e, true)
	testutils.Equal(t, len(values), 3)
	testutils.Equal(t, values["id"], "123")
	testutils.Equal(t, values["name"], "max")
	testutils.Equal(t, values["type"], "request")
	r, values, e = i.Get(http.MethodGet, "/address/max/123")
	testutils.Equal(t, r != nil, true)
	testutils.Equal(t, e, true)
	testutils.Equal(t, len(values), 2)
	testutils.Equal(t, values["id"], "123")
	testutils.Equal(t, values["name"], "max")
	r, _, e = i.Get(http.MethodGet, "/address/max/123/1/2")
	testutils.Equal(t, r != nil, false)
	testutils.Equal(t, e, false)
	r, _, e = i.Get(MethodAny, "/address/max")
	testutils.Equal(t, r != nil, false)
	testutils.Equal(t, e, false)
	r, values, e = i.Get(http.MethodPost, "/s/test.png")
	testutils.Equal(t, r != nil, true)
	testutils.Equal(t, e, true)
	testutils.Equal(t, len(values), 1)
	testutils.Equal(t, values["*"], "test.png")
	r, values, e = i.Get(http.MethodPost, "/s/arxhive/test.png")
	testutils.Equal(t, r != nil, true)
	testutils.Equal(t, e, true)
	testutils.Equal(t, len(values), 1)
	testutils.Equal(t, values["*"], "arxhive,test.png")

	r, _, e = i.Get(http.MethodGet, "/")
	testutils.Equal(t, r != nil, false)
	testutils.Equal(t, e, false)
	i.Add("/", NewRoute(http.MethodGet, hello))
	r, values, e = i.Get(http.MethodGet, "/")
	testutils.Equal(t, r != nil, true)
	testutils.Equal(t, e, true)
	testutils.Equal(t, len(values), 0)
	r, _, e = i.Get(MethodAny, "/a")
	testutils.Equal(t, r != nil, false)
	testutils.Equal(t, e, false)
}
