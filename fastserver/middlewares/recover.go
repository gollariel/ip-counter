package middlewares

import (
	"net/http"

	"bitbucket.org/gollariel/ip-counter/fastlog"
	"github.com/valyala/fasthttp"
)

// Recover recover after panic
func Recover(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		defer func() {
			if rval := recover(); rval != nil {
				fastlog.Error("Recovered request panic", "rval", rval)
				ctx.Error("Panic during the request", http.StatusInternalServerError)
			}
		}()
		next(ctx)
	}
}
